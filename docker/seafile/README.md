## 依赖安装
```shell
pip3 install moviepy
```

https://forum.seafile.com/t/video-thumbnails-not-workinf/5757

## 删除已存在的索引
```
./pro/pro.py search --clear
```

## 创建并且再次更新搜索索引
```
./pro/pro.py search --update
```

## 参考

[常见问题解答](https://www.kancloud.cn/kancloud/seafile-manual/51457)