package io.github.harvies.charon.spring.boot.webflux;

public class Constants {
    
    public final static String SUCCESS = "success";

    public final static String FAIL = "fail";
}
