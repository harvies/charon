package io.github.harvies.charon.cache.request;

import lombok.Data;

import java.util.List;

@Data
public class TestRequest {
    private Integer a;
    private String b;
    private List<Integer> c;
}
