package io.github.harvies.charon.shardingsphere.jdbc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.github.harvies.charon.shardingsphere.jdbc.entity.OrderItem;

public interface OrderItemMapper extends BaseMapper<OrderItem> {
}
