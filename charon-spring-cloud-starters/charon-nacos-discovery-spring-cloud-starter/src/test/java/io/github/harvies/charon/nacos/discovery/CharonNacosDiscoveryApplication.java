package io.github.harvies.charon.nacos.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharonNacosDiscoveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(CharonNacosDiscoveryApplication.class, args);
    }

}
