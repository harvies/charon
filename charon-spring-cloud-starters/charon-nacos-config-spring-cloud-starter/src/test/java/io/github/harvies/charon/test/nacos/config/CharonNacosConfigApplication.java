package io.github.harvies.charon.test.nacos.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharonNacosConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(CharonNacosConfigApplication.class, args);
    }

}
